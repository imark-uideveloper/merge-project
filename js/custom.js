jQuery(document).ready(function() {
  jQuery('.footer-slider').slick({
    slidesToShow: 5,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 2000,
    draggable: true,
    loop: true,
    dots: false,
    arrows: false,
  });

  jQuery('.testimonialSlider').slick({
    dots: true,
    infinite: false,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    arrows: false,
    draggable: true,
    autoplay: true,
    autoplaySpeed: 3000,
  });

  jQuery('.productSlider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    dots: true,
  });

  jQuery('.items-wrapper').isotope({
    itemSelector: '.item',
    filter: "*"
  });

  jQuery('.gallery-menu  a').click(function() {
    var selector = $(this).attr('data-filter');

    jQuery('.items-wrapper').isotope({
      filter: selector,
    })

    //changing active class with click event
    jQuery('.gallery-menu .dropdown-menu a.active').removeClass('active');
    jQuery(this).addClass('active');
  });

});



jQuery(function() {
  //----- OPEN
  jQuery('[data-popup-open]').on('click', function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-open');
    jQuery('[data-popup="' + targeted_popup_class + '"]').fadeIn(100);

    e.preventDefault();
  });

  //----- CLOSE
  jQuery('[data-popup-close]').on('click', function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-close');
    jQuery('[data-popup="' + targeted_popup_class + '"]').fadeOut(200);

    e.preventDefault();
  });
});

wow = new WOW({
  mobile: false, // default
})
wow.init();


/** Google Map **/
